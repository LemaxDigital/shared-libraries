import groovy.json.JsonOutput
import static groovy.json.JsonOutput.*

def call(Map env_config){

	pipeline {
        agent any

        stages {
            stage('Building') {
                steps {
                    echo "Building.. ${env_config.app_name}"
                }
            }
            stage('Unit Testing') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('SonarQube Analysis') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Shiftleft Analysis') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Push to Nexus') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Build Docker image') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Push to Quay') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Deploy') {
                when {
                    expression {
                        currentBuild.result == null || currentBuild.result == 'SUCCESS' 
                    }
                }
                steps {
                    echo 'Deploying....'
                }
            }
        }
    }
}